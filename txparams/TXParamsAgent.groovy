#!/usr/bin/env groovy
import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('net.sf.opencsv:opencsv:2.3')
@Grab(group='org.jsoup', module='jsoup', version='1.6.2')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')

import org.jsoup.Jsoup
import org.jsoup.nodes.*
import org.jsoup.select.*
import static groovy.json.JsonOutput.*
import groovy.json.JsonOutput
import org.ini4j.*;
import groovy.json.JsonOutput;
import java.util.regex.*;
import groovy.json.JsonSlurper
import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import java.security.MessageDigest
import static groovyx.net.http.HttpBuilder.configure
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer


if ( args.length == 0 )
  System.exit(1);

if ( args[0] == null )
  System.exit(1);
  
String required_config = args[0]

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.agents/radiodns'));
String keycloak_url = ini.get(required_config, 'url', String.class);
String keycloak_realm = ini.get(required_config, 'realm', String.class);
String keycloak_user = ini.get(required_config, 'username', String.class);
String keycloak_pass = ini.get(required_config, 'password', String.class);
String target_url = ini.get(required_config, 'rr_target_url', String.class);

println("Running agent with config ${required_config} - ${target_url}");

if ( target_url == null )
  System.exit(1);

Map config = getConfig(required_config)
long runtime = System.currentTimeMillis();

def keycloak = configure {
  request.uri = keycloak_url
}

String session_jwt = login(keycloak, 'selfservice',keycloak_realm,keycloak_user,keycloak_pass);

println("JWT will be ${session_jwt}");


config.lastRunStr=new Date(runtime).toString()
config.lastRun=runtime

secureGet("${target_url}/loadDataFromUrl", session_jwt, [ source: 'https://www.ofcom.org.uk/__data/assets/file/0018/91305/TxParamsDAB.csv' ])

// checkForChanges(config, 'https://www.radiodns.uk/services.json', session_jwt, target_url);
writeConfig(config, required_config);

System.exit(0);

private void secureGet(String url, String bearer, Map params) {
  def builder = configure{
    request.uri = url
    request.headers['accept']='application/json'
    request.headers['Authorization'] = "Bearer $bearer".toString()
    request.contentType='application/json'
  }

  def post_result = builder.get {
    request.uri.query = params
    response.when(200) { FromServer fs, Object rbody ->
      println("RESPONSE:: ${rbody}");
    }

    response.failure { FromServer fs, Object rbody ->
      println("Problem ${rbody} ${fs} ${fs.getStatusCode()}");
    }
  }
}

private void securePost(String url, String bearer, Map body) {
  def builder = configure{
    request.uri = url
    request.headers['accept']='application/json'
    request.headers['Authorization'] = "Bearer $bearer".toString()
    request.contentType='application/json'
    request.body = body
  }

  def post_result = builder.post {
    response.when(200) { FromServer fs, Object rbody ->
      println("RESPONSE:: ${body}");
    }

    response.failure { FromServer fs, Object rbody ->
      println("Problem ${rbody} ${fs} ${fs.getStatusCode()}");
    }
  }
}


private Map getConfig(String required_config) {
  def jsonSlurper = new JsonSlurper()
  Map mappings = [:]
  Map result = null;

  try {
    File params_file = new File("./${required_config}-OFCOMTxParams.json".toString());
    if ( params_file.exists() ) {
      FileReader reader = new FileReader("./${required_config}-OFCOMTxParams.json".toString())
      result = jsonSlurper.parse(reader);
    }
  } catch (Exception e) {
    e.printStackTrace()
  }

  if ( result == null ) {
    result = [
      serviceRegister:[:],
      runLog:[]
    ]
  }

  return result;
}

private void writeConfig(config, required_config) {
  try {
    File params_file = new File("./${required_config}-OFCOMTxParams.json".toString())
    if ( params_file.exists() ) {
      println("Backing up old config file");
      params_file.renameTo("./${required_config}-OFCOMTxParams.json.bak".toString())
    }

    params_file << JsonOutput.prettyPrint(JsonOutput.toJson(config))
  }
  catch ( Exception e ) {
    e.printStackTrace();
  }
}

public String login(HttpBuilder keycloak, String client, String realm, String username, String password) {

  String result = null;
  println("Attempt login using client ${client}, realm ${realm}, user ${username},...");

  def http_res = keycloak.post {
    request.uri.path = "/auth/realms/${realm}/protocol/openid-connect/token".toString()
    request.contentType='application/x-www-form-urlencoded'
    request.body=[
      client_id:client,
      username:username,
      password:password,
      grant_type:'password'
    ]

    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
      result = body.access_token
    }

    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }

  this.jwt = result;
  return result;
}

