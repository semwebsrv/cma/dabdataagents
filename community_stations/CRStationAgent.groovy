#!/usr/bin/env groovy
import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('net.sf.opencsv:opencsv:2.3')
@Grab(group='org.jsoup', module='jsoup', version='1.6.2')

import org.jsoup.Jsoup
import org.jsoup.nodes.*
import org.jsoup.select.*
import static groovy.json.JsonOutput.*
import groovy.json.JsonOutput
import org.ini4j.*;
import groovy.json.JsonOutput;
import java.util.regex.*;
import groovy.json.JsonSlurper
import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import java.security.MessageDigest

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.agents/ofcom'));
String cr_stations_url = ini.get('ofcom', 'cr_stations_url', String.class);
println "process url ${cr_stations_url}"

checkDataDir()
Map config = getConfig()
long runtime = System.currentTimeMillis();

config.lastRunStr=new Date(runtime).toString()
config.lastRun=runtime

checkForChanges(config, cr_stations_url);
writeConfig(config);

publishIfNeeded(config);

System.exit(0);

private void publishIfNeeded(Map config) {
  if ( ( config.state.added > 0 ) || ( config.state.updated > 0 ) ) {
    try {
      File crjson = new File("./ofcom_data/ofcom_community_radio_stations.json")
      crjson << JsonOutput.prettyPrint(JsonOutput.toJson(config))

      List<String[]>rows = new ArrayList();
      rows.add( [ 'slug', 'name', 'on_air_from', 'on_air_to','licencee','addresss','telephone','website','email','license','last_update' ] as String[] )
      config.stationRegister.collect { k, v ->
        rows.add( [ k, v.name, v.on_air_from, v.on_air_to, v.licensee,
                    v.address?.join(', '),
                    v.telephone, v.website, v.email, v.license, v.ofcom_last_updated ] as String[] )
      }
      FileOutputStream fos = new FileOutputStream('./ofcom_data/ofcom_community_radio_stations.csv')
      OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
      CSVWriter csvWriter = new CSVWriter(osw)
      csvWriter.writeAll(rows)
      csvWriter.close()
      osw.close()
      fos.close()
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
  }
}

private void checkForChanges(Map config, String cr_stations_url) {

  Document doc = Jsoup.connect(cr_stations_url).get();

  config.state = [
    added:0,
    updated:0,
    unchanged:0
  ]

  Elements stations = doc.select("h2:contains(List of Community Radio Stations)").first().parent().parent().getElementsByClass('body').first().children()
  stations.each { s ->
    Element link = s.select('h3 > a').first()
    if ( link != null ) {
      def stationpage = link.attr('href').trim()
      def stationname = link.text().trim();
      println("${stationname}:${stationpage}");
      checkStation(stationpage, stationname, config);
    }
  }
}

private boolean checkStation(String page, String name, Map config) {

  boolean changed = false;

  Document doc = Jsoup.connect('http://static.ofcom.org.uk/static/radiolicensing/html/radio-stations/community/'+page).get()
  Element content = doc.select('div#Content').first()
  Element name_content = content.getElementsByClass('hero').first().select('h1').first()
  String station_name = name_content.text()
  println("station name: ${station_name}");
  Map station_data = [
    name: station_name
  ]
  String station_slug = null;

  Element body = content.select('div.body').first()
  int counter = 0;
  body.select('p').each { station_assertion ->

    String para_text = station_assertion.text();
    boolean has_contact_details = false;

    if ( counter == 0 ) {
      // A blank P usually
    }
    if ( counter == 1 ) {
      // We can only guess - first p contains frequency
    }
    if ( ( counter == 2 ) && ( para_text.startsWith('On Air From: ') ) ) {
      station_data['on_air_from'] = para_text.substring(13).trim()
    }
    if ( ( counter == 3 ) && ( para_text.startsWith('to: ') ) ) {
      station_data['on_air_to'] = para_text.substring(4).trim()
    }
    if ( counter == 4 ) { 
      if ( para_text.startsWith('Licensee: ') ) {
        station_data['licensee'] = para_text.substring(10).trim()
      }
      else {
        println("ERROR: Licensee was expected in 3rd para of station content but found ${para_text}");
      }
    }
    if ( ( counter == 5 ) && ( para_text.equals('Contact Details:') ) ) {
      has_contact_details = true;
    }

    if ( counter > 5 ) {
      // After line 5 seems to be address info with some special cases like Website: Email: License Number
      if ( para_text.startsWith('Telephone: ') ) {
        station_data['telephone'] = para_text.substring(10).trim()
      }
      else if ( para_text.startsWith('Website: ') ) {
        station_data['website'] = para_text.substring(9).trim()
      }
      else if ( para_text.startsWith('Email: ') ) {
        station_data['email'] = para_text.substring(7).trim()
      }
      else if ( para_text.startsWith('Licence Number: ') ) {
        station_data['license'] = para_text.substring(16).trim()
        station_slug = station_data['license'].split('/')[0]
      }
      else if ( para_text.startsWith('Key Comm') ) {
      }
      else if ( para_text.startsWith('View Coverage') ) {
      }
      else if ( para_text.startsWith('Last Updated : ') ) {
        station_data['ofcom_last_updated'] = para_text.substring(15).trim()
      }
      else {
        if ( station_data.address == null )
          station_data.address = []
        station_data.address.add(para_text.trim());
      }
    }
      
    counter++;
  }

  if ( station_slug != null ) {
    String station_data_as_json = JsonOutput.toJson(station_data)
    println("Station data: ${station_data_as_json}");

    MessageDigest md5_digest = MessageDigest.getInstance("MD5");
    md5_digest.update(station_data_as_json.getBytes())
    byte[] md5sum = md5_digest.digest();
    String newhash = new BigInteger(1, md5sum).toString(16);

    def stn_in_reg = config.stationRegister[station_slug];
    if ( stn_in_reg == null ) {
      // Adding station to register for first time
      station_data.checksum = newhash;
      config.stationRegister[station_slug] = station_data
      config.state.added++;
      changed = true
    }
    else {
      println("Existing registry entry found....");
      if ( stn_in_reg.checksum == newhash ) {
        // Data unchanged
        config.state.unchanged++;
      }
      else {
        station_data.checksum = newhash;
        config.stationRegister[station_data.license] = station_data
        config.state.updated++;
        changed = true
      }
    }
  }
  else {
    println("ERROR: station does not have a license");
  }

  return changed
}


private void checkDataDir() {
  File datadir = new File('./ofcom_data')
  if ( !datadir.exists() ) {
    datadir.mkdir()
  }
}


private Map getConfig() {
  def jsonSlurper = new JsonSlurper()
  Map mappings = [:]
  Map result = null;

  try {
    File params_file = new File('./CRStations.json');
    if ( params_file.exists() ) {
      FileReader reader = new FileReader("./CRStations.json")
      result = jsonSlurper.parse(reader);
    }
  } catch (Exception e) {
    e.printStackTrace()
  }

  if ( result == null ) {
    result = [
      stationRegister:[:],
      runLog:[]
    ]
  }

  return result;
}

private void writeConfig(config) {
  try {
    File params_file = new File("./CRStations.json")
    if ( params_file.exists() ) {
      params_file.renameTo('./CRStations.json.bak')
    }

    params_file << JsonOutput.prettyPrint(JsonOutput.toJson(config))
  }
  catch ( Exception e ) {
    e.printStackTrace();
  }
}
